<!DOCTYPE html>
<html>
    <head>
        <!-- meta -->        
        <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />         
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- title -->
        <title><?php wp_title( '|', true); ?></title>
        <meta name="robots" content="noindex,follow,noodp" />
        
        <link rel="profile" href="http://gmpg.org/xfn/11" />                
        
        <!-- global javascript variable -->
        <script type="text/javascript">
            var CONTAINER_WIDTH = '1090px';
            var CONTENT_WIDTH = '1060px';
            var BASE_URL = '<?php bloginfo('url'); ?>';
            var TEMPLATE_URI = '<?php bloginfo('template_url') ?>';
            var CURRENT_MODULE_URI = '';
            Date.now = Date.now || function() { return +new Date; };            
        </script>        
        <!-- Bootstrap -->
        <link href="<?php bloginfo('template_url'); ?>/css/bootstrap.min.css" rel="stylesheet" />
        <link href="<?php bloginfo('template_url'); ?>/css/bootstrap-theme.min.css" rel="stylesheet" />   
        <!-- fontawesome -->
        <link href="<?php bloginfo('template_url'); ?>/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="<?php bloginfo('template_url'); ?>/js/html5shiv.js"></script>
        <script src="<?php bloginfo('template_url'); ?>/js/respond.min.js"></script>
        <![endif]-->        
        
        <script src="<?php bloginfo('template_url'); ?>/js/jquery.js" type="text/javascript"></script>		
		<script src="<?php bloginfo('template_url'); ?>/js/jquery.vticker.min.js" type="text/javascript"></script>        		
        <script src="<?php bloginfo('template_url'); ?>/js/jquery.plugins.js" type="text/javascript"></script>               
        <script src="<?php bloginfo('template_url'); ?>/js/bootstrap.min.js" type="text/javascript"></script>        		
        <link href="<?php bloginfo('template_url'); ?>/style.css?<?php echo md5(date('l jS \of F Y h:i:s A')); ?>" rel="stylesheet" />
        <script src="<?php bloginfo('template_url'); ?>/js/config.js" type="text/javascript"></script>   
		<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>		
        <?php wp_head(); ?>
    </head>
    <body>     
        <div id="screen_type"></div>
        
        <div id="wrapper"><!-- begin wrapper -->

            <header><!-- begin header -->
				<div class="wrapper clearfix">
                    <div class="logo">
						<h1 class="top-text">ホームページ制作のことならWebサイト制作ドットコム</h1>
                        <?php if(is_front_page()) : ?>
							<a href="<?php bloginfo('url'); ?>"><img src="<?php bloginfo('template_url'); ?>/img/common/logo.jpg" alt="<?php bloginfo('name'); ?>" /></a>
						<?php else : ?>
							<a href="<?php bloginfo('url'); ?>"><img src="<?php bloginfo('template_url'); ?>/img/common/logo.jpg" alt="<?php bloginfo('name'); ?>" /></a>
						<?php endif; ?>
                    </div><!-- end logo --->
                    <div class="header-tel">
                        <img src="<?php bloginfo('template_url'); ?>/img/common/header_tel.jpg" alt="top" />
                    </div> <!-- end header-tel --> 
					<div class="header-con">
						<a href="<?php bloginfo('url'); ?>/contact">
							<img src="<?php bloginfo('template_url'); ?>/img/common/header_con.jpg" alt="top"/>
						</a>                        
                    </div> <!-- end header-con -->
                </div><!-- end wrapper -->			
            </header><!-- end header -->
            
			<section id="slider">
				<div class="slider-content">
					<img src="<?php bloginfo('template_url'); ?>/img/common/slider_banner.jpg" alt="top"/>
				</div>				
			</section><!-- end slider -->
			
			<section id="top-content1">
				<div class="wrapper clearfix">
					<div class="top-content1-text">HP制作に関わる全て<span class="text-small">をお任せ下さい</span></div>
				</div>
			</section><!-- end top-content1 -->
            
			<section id="top-content2">
				<div class="wrapper clearfix">
					<div class="top-content-info1">
						<img src="<?php bloginfo('template_url'); ?>/img/common/top_img_computer.png" alt="top"/>
					</div>
					<div class="top-content-info2">
						<img src="<?php bloginfo('template_url'); ?>/img/common/top_img_items.jpg" alt="top"/>
					</div>
				</div>
			</section><!-- end top-content2 -->
			
			<section id="top-content3">
				<div class="wrapper clearfix">
					<ul class="top-content3-info">
						<li>
							<p class="top-content3-text">電話によるお問い合わせ</p>
							<p><img src="<?php bloginfo('template_url'); ?>/img/common/main_img_tel.jpg" alt="top"/></p>
						</li>
						<li>
							<p class="top-content3-text">メールによるお問い合わせ</p>
							<p>
								<a href="<?php bloginfo('url'); ?>/contact">
									<img src="<?php bloginfo('template_url'); ?>/img/common/main_img_con.jpg" alt="top"/>
								</a>
							</p>
						</li>
					</ul>					
				</div>				
			</section><!-- end top-content3 -->
			
            <section id="content"><!-- begin content -->                
                <main class="primary"><!-- begin primary -->
       