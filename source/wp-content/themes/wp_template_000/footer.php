                </main><!-- end primary -->                                                             
            </section><!-- end content -->

            <footer><!-- begin footer -->
                <div class="footer-content1">
					<div class="wrapper clearfix">
						<ul>
							<li>個人情報の取り扱いについて</li>
							<li>運営会社</li>
						</ul>
					</div>
				</div><!-- end footer-content1 -->
				
				<div class="footer-content2 wrapper clearfix">
					<div class="footer-cont2-left">
						<p class="footer-cont2-text">ホームページ制作のことならWebサイト制作ドットコム</p>
						<img src="<?php bloginfo('template_url'); ?>/img/common/footer_logo.jpg" alt="top" />
					</div>
					<div class="footer-cont2-right">
						<img src="<?php bloginfo('template_url'); ?>/img/common/footer_tel.jpg" alt="top" />
					</div>
				</div><!-- end footer-content2 -->
				
				<div class="footer-copyright wrapper">
					© 2015 Copyright. <span class="copyright-text">株式会社RESEED</span>
				</div><!-- end footer-copyright -->			
            </footer><!-- end footer -->            
        </div><!-- end wrapper -->
        <?php wp_footer(); ?>
		<!-- Js plugins -->
				<script src="http://ajaxzip3.googlecode.com/svn/trunk/ajaxzip3/ajaxzip3.js" charset="UTF-8"></script>
				<script type="text/javascript">
					$(document).ready(function(){
						$('#content1').vTicker('init',{
							speed: 800, 
							pause: 1000,
							showItems: 5,				
						});	
						
						$('#zip').change(function(){					
							//AjaxZip3.JSONDATA = "https://ajaxzip3.googlecode.com/svn/trunk/ajaxzip3/zipdata";
							AjaxZip3.zip2addr(this,'','addr1');
						});
						
						/* Every time the window is scrolled ... */
						$(window).scroll( function(){						
							/* Check the location of each desired element */
							$('.hideme').each( function(i){								
								var bottom_of_object = $(this).offset().top + $(this).outerHeight();
								var bottom_of_window = $(window).scrollTop() + $(window).height();								
								/* If the object is completely visible in the window, fade it it */
								if( bottom_of_window > bottom_of_object ){          
									$(this).fadeIn('slow').animate({'opacity':'0.7'}, 1000);
								}								
							}); 						
						});
					
					});
								 
					$(function(){	
						$('#main-content4-list').each(function(){
							var loopsliderWidth = $(this).width();
							var loopsliderHeight = $(this).height();
							$(this).children('ul').wrapAll('<div id="main-content4-list-bg"></div>');
					 
							var listWidth = $('#main-content4-list-bg').children('ul').children('li').width();
							var listCount = $('#main-content4-list-bg').children('ul').children('li').length;
					 
							var loopWidth = (listWidth)*(listCount);
					 
							$('#main-content4-list-bg').css({
								top: '0',
								left: '0',
								width: ((loopWidth) * 2),
								height: (loopsliderHeight),
								overflow: 'hidden',
								position: 'absolute'
							});
					 
							$('#main-content4-list-bg ul').css({
								width: (loopWidth)
							});
							loopsliderPosition();
					 
							function loopsliderPosition(){
								$('#main-content4-list-bg').css({left:'0'});
								$('#main-content4-list-bg').stop().animate({left:'-' + (loopWidth) + 'px'},8000,'linear');
								setTimeout(function(){
									loopsliderPosition();
								},8000);
							};
					 
							$('#main-content4-list-bg ul').clone().appendTo('#main-content4-list-bg');
						});
					});					 
				</script>
    </body>
</html>