<?php get_header(); ?>
	<div class="main-content1 clearfix">
		<div class="main-content1-list wrapper" id="content1">
			<ul>   
				<li>集客したいがどこに相談したらいいのか分からない</li>
				<li>社内に知識のある人間がいないので不安</li>
				<li>海外に向けて商品を販売していきたい</li>
				<li>ネットから注文が来た時の対応方法までサポートして欲しい</li>
				<li>ネットは初めてなので、不安</li>
		   </ul>
		</div>		
	</div><!-- end main-content1 -->   
	
	<div class="main-content2 clearfix">
		<div class="wrapper">
			<div class="main-content2-left hideme">
				<img src="<?php bloginfo('template_url'); ?>/img/content/top_img_guide1.png" alt="top"/>
			</div>
			<div class="main-content2-right hideme">
				<img src="<?php bloginfo('template_url'); ?>/img/content/top_img_guide2.png" alt="top"/>
			</div>	
		</div>		
	</div><!-- end main-content2 -->
	
	<div class="main-content3 clearfix">
		<div class="wrapper">
			<img src="<?php bloginfo('template_url'); ?>/img/content/top_content_img.jpg" alt="top"/>
		</div>
	</div><!-- end wrapper -->
	
	<div class="main-content4 clearfix">
		<div class="wrapper clearfix">
			<div class="main-content4-title">
				<div id="topselect">
					<img src="<?php bloginfo('template_url'); ?>/img/content/top_content_select.jpg" alt="top"/>
				</div>			
			</div>		
		</div><!-- end wrapper -->	
		<div class="main-content4-info">
			<div id="main-content4-list">
			<?php
			$performance_posts = get_posts( 	array(
				'post_type'=> 'performance',			
			));
			?>
			<?php $i = 0; ?>
			<ul>
				<?php foreach($performance_posts as $p) :  ?>
					<li>
					<?php echo get_the_post_thumbnail($p->ID,'medium'); ?>
					<h2 class="main-content4-title"><?php echo $p->post_title; ?></h2>
					<div class="main-content4-text"><?php echo $p->post_content;?></div>
				</li>	
				<?php endforeach; ?>			
			</ul>
			<?php wp_reset_query(); ?>
			</div><!-- main-content5-list -->
		</div><!-- end main-content4-info -->		
	</div><!-- end main-content4  -->
	
	<div class="main-content5 clearfix">
		<div class="main-content5-info">
			<ul class="main-content5-list wrapper"> 
				<li>お問い合わせ<span class="main-content6-clr">や</span>購入者<span class="main-content6-clr">の</span>質が上がる</li>
				<li>繁忙期<span class="main-content6-clr">や閑散期の</span>差が埋まる</li>
				<li><span class="main-content6-clr">市場の</span>ニーズ<span class="main-content6-clr">が</span>集まる</li>
				<li>見込顧客<span class="main-content6-clr">の</span>リスト<span class="main-content6-clr">が</span>集まる</li>
			</ul>
		</div>		
	</div><!-- end main-content5 -->
	
	<div class="main-content6 clearfix">
		<div class="main-content6-info wrapper"> 
			<p>Webからの集客、売り上げUPに<br />課題を持つお客様から選ばれる<br />最大の理由です！！</p>
		</div>
	</div><!-- end main-content6 -->

	<div class="main-content7 clearfix">
		<div class="main-contact-info wrapper">
			<div class="main-con-title">お問い合わせ</div>
			<div class="main-con-line">
				<div class="main-con-lleft">
					<img src="<?php bloginfo('template_url'); ?>/img/content/top_line_img1.jpg" alt="top" />
				</div>
				Contact
				<div class="main-con-lright">
					<img src="<?php bloginfo('template_url'); ?>/img/content/top_line_img2.jpg" alt="top" />
				</div>				
			</div>
			<div class="main-con-info clearfix">
				<div class="main-con-left">
					<p class="conl-text1">ますはお気軽にご相談ください。</p>
					<div class="conl-text2">
						<p>お急ぎの方は、お電話よりお問い合わせください。</p>
						<p>メールでのお問い合わせは、下記お問い合わせフォームをご利用ください。</p>
					</div>
				</div>
				<div class="main-con-right">
					<p class="conr-text">電話によるお問い合わせ</p>
					<img src="<?php bloginfo('template_url'); ?>/img/content/top_form_tel.jpg" alt="top" />
				</div>
			</div>			
			<div class="contact-form">			
				<?php echo do_shortcode('[contact-form-7 id="248" title="お問い合わせ"]') ?>						
			</div>
		</div><!-- main-contact-info -->		
	</div><!-- end main-conten7 -->
<?php get_footer(); ?>		

<script type="text/javascript">
	function interval() {
	$('#topselect').effect('shake', { times:2 }, 20);
	}
	$(document).ready(function() {  
		var shake = setInterval(interval, 500);    
	});	
</script>
	